Originally exported from Blender via io_scene_multi:
https://bitbucket.org/wahn/blender-add-ons/src/master/
Ships now with rs-pbrt:
https://www.rs-pbrt.org/about/
Can be rendered via Arnold:
https://www.arnoldrenderer.com/
or the Rust version of pbrt-v3:
https://github.com/wahn/rs_pbrt

D:\Autodesk\Arnold-6.1.0.0\bin\kick.exe -dp -dw -v 6 -set MALight_light.intensity 1 -set MALight_light.samples 3 -set options.GI_diffuse_samples 6 .\cornell_box.ass
D:\Work\Rendering\git\rs_pbrt\target\release\examples\parse_ass_file.exe .\cornell_box.ass
