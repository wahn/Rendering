+++
title = "About"
description = "Rendering"

date = 2020-11-23
template = "page.html"
+++

This site is build with [Zola](https://www.getzola.org), a
[Rust](https://www.rust-lang.org)-based [static-site
generator](https://jamstack.org/generators).
