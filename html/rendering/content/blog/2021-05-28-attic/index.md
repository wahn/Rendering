+++
title = "Attic"
description = "Attic"

date = 2021-05-28
template = "page.html"
+++

# Houdini

## Mantra

![The Attic scene within Houdini](./attic_mantra_houdini_001.png)

![Rendered via Houdini and Mantra](./attic_mantra_houdini_002.png)

[attic_mantra.zip](https://www.janwalter.org//Download/Scenes/Rendering/attic/attic_mantra.zip)
