+++
title = "Cornell Box"
description = "Cornell Box"

date = 2020-11-23
template = "page.html"
+++

# Houdini

## Mantra

![Rendered via Houdini and Mantra](./houdini_mantra.png)

[cornell_box_mantra.zip](https://www.janwalter.org//Download/Scenes/Rendering/cornell_box/cornell_box_mantra.zip)

## Arnold

![Rendered via Houdini and Arnold](./houdini_arnold.png)

[cornell_box_arnold.zip](https://www.janwalter.org//Download/Scenes/Rendering/cornell_box/cornell_box_arnold.zip)

# Blender

## Cycles

![Rendered via Blender and Cycles](./cycles.png)

[cornell_box_cycles.zip](https://www.janwalter.org//Download/Scenes/Rendering/cornell_box/cornell_box_cycles.zip)

## appleseed

![Rendered via Blender and appleseed](./blender_appleseed.png)

[cornell_box_appleseed.zip](https://www.janwalter.org//Download/Scenes/Rendering/cornell_box/cornell_box_appleseed.zip)

## Indigo

![Rendered via Blender and Indigo](./blendigo.png)

[cornell_box_indigo.zip](https://www.janwalter.org//Download/Scenes/Rendering/cornell_box/cornell_box_indigo.zip)

## LuxCoreRender

![Rendered via Blender and LuxCoreRender](./blender_luxcorerender.png)

[cornell_box_luxcorerender.zip](https://www.janwalter.org//Download/Scenes/Rendering/cornell_box/cornell_box_luxcorerender.zip)

## OctaneRender

![Rendered via OctaneRender](./blender_octanerender.png)

[cornell_box_octanerender.zip](https://www.janwalter.org//Download/Scenes/Rendering/cornell_box/cornell_box_octanerender.zip)

# Standalone

## Guerilla

![Rendered via Guerilla 2.2.7](./guerilla.png)

[cornell_box_guerilla.zip](https://www.janwalter.org//Download/Scenes/Rendering/cornell_box/cornell_box_guerilla.zip)

## Maxwell

![Rendered via Maxwell](./maxwell.png)

[cornell_box_maxwell.mxs](https://www.janwalter.org//Download/Scenes/Rendering/cornell_box/cornell_box_maxwell.mxs)
